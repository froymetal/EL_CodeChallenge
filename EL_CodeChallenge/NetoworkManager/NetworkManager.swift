//
//  NetworkManager.swift
//  EL_CodeChallenge
//
//  Created by Froy on 7/13/22.
//

import Foundation

protocol IpManagerDelegate {
    func getIp(ip: IpData)
}

struct NetworkManager {

    let ipUrl = "https://ip.jsontest.com"

    var delegate: IpManagerDelegate?

    func performRequest (urlString: String) {
        if let url = URL(string: urlString) {
            let session = URLSession(configuration: .default)

            let task = session.dataTask(with: url) { data, response, error in
                if error != nil {
                    return
                }
                if let safeData = data {
//                    let dataString = String(data: safeData, encoding: .utf8)
//                    print(dataString)
//                    self.parseJSON(ipData: safeData)
                    if let ip = self.parseJSON(ipData: safeData) {
//                        self.delegate?.getIp(ip: ip)
                    }
                }
            }
            task.resume()
        }
    }

    func parseJSON(ipData: Data) -> IpViewModel? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(IpData.self, from: ipData)
            let ip = decodedData.ip

            let ipViewModel = IpViewModel(ip: ip)

            print(decodedData.ip)
            return ipViewModel
        } catch {
            print(error)
            return nil
        }
    }

}
