//
//  IpData.swift
//  EL_CodeChallenge
//
//  Created by Froy on 7/13/22.
//

import Foundation

struct IpData: Decodable {
    let ip: String
}
